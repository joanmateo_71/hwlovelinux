Informacio d'extraccio de hardware de l'ordinador de l'aula
Data de l'extraccio: 16/Nov/2020
Nom: Joan Mateo 

# Extract hardware info from host with linux
## system

### system model, bios version and bios date, how old is the hardware?
* Pots extraure el system model amb **dmidecode -t system**
```
[root@a02 ~]# dmidecode -t system
# dmidecode 3.2
Getting SMBIOS data from sysfs.
SMBIOS 2.7 present.

Handle 0x0001, DMI type 1, 27 bytes
System Information
	Manufacturer: Gigabyte Technology Co., Ltd.
	Product Name: H81M-S2PV
	Version: To be filled by O.E.M.
	Serial Number: To be filled by O.E.M.
	UUID: 03d40274-0435-05ce-5b06-8a0700080009
	Wake-up Type: Power Switch
	SKU Number: To be filled by O.E.M.
	Family: To be filled by O.E.M.

Handle 0x0028, DMI type 12, 5 bytes
System Configuration Options
	Option 1: To Be Filled By O.E.M.

Handle 0x0029, DMI type 32, 20 bytes
System Boot Information
	Status: No errors detected
```

* BIOS Version: **dmidecode -s bios-version**
```
[root@a02 ~]# dmidecode -s bios-version
FA
```

* BIOS Date: **dmidecode -s bios-date**
```
[root@a02 ~]# dmidecode -s bios-release-date
04/17/2014
```

## mainboard model, link to manual, link to product
* command: **dmidecode -s baseboard-model**
```
[root@a02 ~]# dmidecode -s baseboard-manufacturer
Gigabyte Technology Co., Ltd.
[root@a02 ~]# dmidecode -s baseboard-product-name
H81M-S2PV
```
* link al producte: http://es.gigabyte.com/products/page/mb/ga-h81m-s2pvrev_10#kf
* link al manual: http://es.gigabyte.com/products/page/mb/ga-h81m-s2pvrev_10#support-manual

### memory banks (free or occupied)

You can see it with the command **lshw -class memory**
```
root@a02 ~]# lshw -class memory
[...]
 *-memory
       description: System Memory
       physical id: 7
       slot: System board or motherboard
       size: 8GiB
     *-bank:0
          description: DIMM [empty]
          product: [Empty]
          vendor: [Empty]
          physical id: 0
          serial: [Empty]
          slot: ChannelA-DIMM0
     *-bank:1
          description: DIMM [empty]
          product: [Empty]
          vendor: [Empty]
          physical id: 1
          serial: [Empty]
          slot: ChannelA-DIMM1
     *-bank:2
          description: DIMM DDR3 Synchronous 1400 MHz (0.7 ns)
          product: 99U5471-066.A00LF
          vendor: Kingston
          physical id: 2
          serial: 110ECA05
          slot: ChannelB-DIMM0
          size: 8GiB
          width: 64 bits
          clock: 1400MHz (0.7ns)
     *-bank:3
          description: DIMM [empty]
          product: [Empty]
          vendor: [Empty]
          physical id: 3
          serial: [Empty]
          slot: ChannelB-DIMM1
```

### how many disks and types can be connected
2 x SATA 6Gb/s connectors (SATA3 0~SATA3 1) supporting up to 2 SATA 6Gb/s devices- 
2 x SATA 3Gb/s connectors (SATA2 2~ SATA2 3) supporting up to 2 SATA 3Gb/s devices

### chipset, link to 
Intel® H81 Express Chipset
* ark link: https://ark.intel.com/content/www/es/es/ark/products/75016/intel-h81-chipset.html
* block diagram image link: https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.intel.la%2Fcontent%2Fwww%2Fxl%2Fes%2Fembedded%2Fproducts%2Fshark-bay%2Fdesktop%2Fspecifications.html&psig=AOvVaw3eis3oUkOh7f6x6eHf3ItZ&ust=1605610859374000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCJjwrNr0hu0CFQAAAAAdAAAAABAD

## cpu


### cpu model, year, cores, threads, cache 


### socket 


## pci


### number of pci slots, lanes available

 
### devices connected

### network device, model, kernel module, speed

### audio device, model, kernel module

### vga device, model, kernel module

## hard disks

### /dev/* , model, bus type, bus speed

### test fio random (IOPS) and sequential (MBps)

### network device, model, kernel module, speed

### audio device, model, kernel module

### vga device, model, kernel module

## hard disks

### /dev/* , model, bus type, bus speed

### test fio random (IOPS) and sequential (MBps)