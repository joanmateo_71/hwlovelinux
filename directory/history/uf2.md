## 10 de nov. de 2020

La comanda **lsblk** és per extreure info de les pariticions, sist., emmagatzematge, i muntatge:

```
[guest@a02 ~]$ lsblk
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda      8:0    0 465.8G  0 disk 
├─sda1   8:1    0     1K  0 part 
├─sda5   8:5    0   100G  0 part /
├─sda6   8:6    0   100G  0 part 
└─sda7   8:7    0     5G  0 part [SWAP]
```

El apartado (-O) muestra TODA la info y el -S el modelo. El -o + nombre de columna muestra columnas específicas

Para crear un comando, se usa **alias**.
sintaxis: newcommand='command'

```
[root@a02 ~]# alias
alias cp='cp -i'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias grep='grep --color=auto'
alias l.='ls -d .* --color=auto'
alias ll='ls -l --color=auto'
alias ls='ls --color=auto'
alias mc='. /usr/libexec/mc/mc-wrapper.sh'
alias mv='mv -i'
alias rm='rm -i'
alias which='(alias; declare -f) | /usr/bin/which --tty-only --read-alias --read-functions --show-tilde --show-dot'
alias xzegrep='xzegrep --color=auto'
alias xzfgrep='xzfgrep --color=auto'
alias xzgrep='xzgrep --color=auto'
alias zegrep='zegrep --color=auto'
alias zfgrep='zfgrep --color=auto'
alias zgrep='zgrep --color=auto'
```

Pasos para crear alias:

```
[root@a02 ~]# alias lsdisk='lsblk -o NAME,MODEL,FSTYPE,SIZE,TYPE,MODEL,MOUNTPOINT'
[root@a02 ~]# lsdisk
NAME   MODEL             FSTYPE   SIZE TYPE MODEL             MOUNTPOINT
sda    ST500DM002-1BD142        465.8G disk ST500DM002-1BD142 
├─sda1                              1K part                   
├─sda5                   ext4     100G part                   /
├─sda6                   ext4     100G part                   
└─sda7                   swap       5G part                   [SWAP]
[root@a02 ~]# 
```

S.M.A.R.T.: Tecnología que analiza el estado del disco duro y se creóó para saber cuando un disco duro podría dejar de funcionar.

para ver estado del disco duro:
```
smartclt -a /dev/sda
```


